This is a Dockerized Devops framework. The purpose is to fast build a robust, scalable and dockerized environment for programmer to create a product soon.

it's including dockerized, distributed mysql cluster, mongo cluster, redis cluster with Go framework beego.
beego will be setup with CI(jenkins), Goconvey(unit test framewrok) and DB migration, package management(Godep).

The environment is managed by swarm and consul used only for service discovery now.

The environment with project "AwsBeansTalk" can deploy to aws beanstalk for load balance and auto scaling.