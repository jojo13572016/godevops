#!/bin/bash
APP_NAME=godepops
MAIN_CONTAINER=$APP_NAME
DB_CONTAINER=${APP_NAME}_mysql
DB_PASSWORD=password
DEFAULT_DB_NAME=backenddb

echo "Trying to cleanup any leftover containers"
#docker stop $DB_CONTAINER && docker rm -v $DB_CONTAINER
docker stop $MAIN_CONTAINER && docker rm -f $MAIN_CONTAINER

#echo "Starting MySQL"
#docker run --name $DB_CONTAINER -e MYSQL_ROOT_PASSWORD="$DB_PASSWORD" -e MYSQL_DATABASE="$DEFAULT_DB_NAME" -d mysql:5.7



docker build -t $APP_NAME --build-arg APP_NAME=$APP_NAME . 
#docker run --name $APP_NAME --rm -v $(pwd):/go/src/$APP_NAME -w /go/src/$APP_NAME $APP_NAME bee new server 
docker run --name $APP_NAME -d -p 3306:3306 -p 8080:8080 $APP_NAME $@
