package main

import (
	_ "godepops/routers"
	"github.com/astaxie/beego"
	"fmt"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)
func init() {
    orm.RegisterDriver("mysql", orm.DRMySQL)
    var dbhost string = "172.31.2.226:3306"
    var dbuser string = "root"
    var dbpassword string = "1234567"
    var db string = "users"

    conn := dbuser + ":" + dbpassword + "@tcp(" + dbhost + ")/" + db + "?charset=utf8"
    // param 4 (optional):  set maximum idle connections
    // param 4 (optional):  set maximum connections (go >= 1.2)
    maxIdle := 50
    maxConn := 50
    orm.RegisterDataBase("default", "mysql", conn, maxIdle, maxConn)
    fmt.Println("start init beego with db " + conn)
}

func main() {
	o := orm.NewOrm()
    	o.Using("users") // Using default, you can use other database

    	profile := new(Profile)
    	profile.Age = 30

    	user := new(User)
    	user.Profile = profile
    	user.Name = "slene"

    	fmt.Println(o.Insert(profile))
    	fmt.Println(o.Insert(user))
	beego.Run()
}
