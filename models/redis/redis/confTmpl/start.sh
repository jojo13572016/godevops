#!/bin/bash
apt-get update
apt-get install -y gettext
envsubst < redis.conf > ../redis-conf/redis.conf
envsubst < supervisord.conf > ../redis-conf/supervisord.conf
