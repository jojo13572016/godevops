#!/bin/bash
echo "Start redis supervisord"
supervisord
sleep 3 

#IP=`ifconfig | grep "inet addr:17" | cut -f2 -d ":" | cut -f1 -d " "`
echo IDX=${IDX} OPTIONS=${OPTIONS}
if [ ${IDX} -eq 1 ]; then
	echo "Start setup redis cluster"
	echo "yes" | ruby /redis/src/redis-trib.rb create ${OPTIONS} 
fi
tail -f /var/log/supervisor/redis-1.log
