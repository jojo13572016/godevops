#!/bin/bash
if [ $# -ne 1 ]
  then
    echo "Arguments Number Wrong, it should be Index"
    exit
fi

for (( i = 0; i <= $1; i++ )); do
        echo "kill redis${i}"
        docker kill redis${i}
        docker rm -f redis${i}
done

ETH0IP=$(/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')
PORT=$((7000+$1))
COM_PORT=$((17000+$1))
echo redis$1 IP=${ETH0IP} PORT=${PORT}

if [ $1 -eq 1 ]; then

OPTION="--replicas 1 ${ETH0IP}:${PORT}"
for (( i = 7002; i < 7100; i++ )); do
	REDIS_DATA=$(echo $(curl -sb -H "Accept: application/json" "http://172.31.13.64:8500/v1/catalog/service/redis-${i}"))
	REDIS_LEN=$(echo ${REDIS_DATA} | jq length)
	if [ ${REDIS_LEN} -eq 0 ]; then
		break
	fi
       	data=$(echo ${REDIS_DATA} | jq ".[0]")
       	TMP_IP=$(echo ${data} | jq -r -c ".ServiceAddress")
       	TMP_PORT=$(echo ${data} | jq -r -c ".ServicePort")
       	OPTION=$(echo ${OPTION} ${TMP_IP}:${TMP_PORT})
done

else
OPTION=""

fi

echo Redis Options =${OPTION}
echo "generate config file from template"
docker run -i --rm -w /confTmpl -v $(pwd)/redis/redis-conf:/redis-conf \
			        -v $(pwd)/redis/confTmpl:/confTmpl \
	               		-e IP=${ETH0IP} \
                		-e PORT=${PORT} \
                		phusion/baseimage:0.9.16 /bin/bash /confTmpl/start.sh
echo Redis$1 ${PORT} ${ETH0IP} $1 ${OPTION}

docker build -t jojo13572001/redis $(pwd)/redis
docker run --name redis$1 -id -p ${PORT}:${PORT} -p ${COM_PORT}:${COM_PORT} \
		   -e OPTIONS="${OPTION}" \
		   -e IP=${ETH0IP} \
           	   -e PORT=${PORT} \
           	   -e IDX=$1 \
		   jojo13572001/redis
