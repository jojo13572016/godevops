#!/bin/bash

if [ $# -ne 3 ]
  then
    echo "Arguments Number Wrong, it should be three: shard_num shard_index ip"
    exit
fi

# Docker interface ip
DOCKERIP=$(/sbin/ifconfig docker0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')
MASTERIP=$3
ETH0IP=$(/sbin/ifconfig eth0 | grep 'inet addr:' | cut -d: -f2 | awk '{ print $1}')
LOCALPATH=$(pwd)
#DNS_HOSTNAME=mongodr$2.mongo.dev.docker
#DNS_NAME=mongodr$2.mongo.dev.docker

#echo "clear mongod DNS"
#docker kill mongodr$2DNS
#docker rm mongodr$2DNS
echo "claer all configserver"
docker rm -f $(sudo docker ps -a -f 'name=configserver' -q)
docker rm -f $(sudo docker ps -a -f 'name=mongos' -q) 

echo "claer all smaller mongod"
for (( i = 1; i <= $1; i++ )); do
for (( j = 1; j <= $2; j++ )); do
	echo "kill mongod${i}r${j}"
	docker kill mongod${i}r${j} 
	docker rm mongod${i}r${j}	
done
done

if [ $2 -eq 1 ]; then
	echo Start generate replication ip for template
	MONGOD1R2=$(echo $(curl -sb -H "Accept: application/json" "http://${MASTERIP}:8500/v1/catalog/service/mongo") \
		| jq -r '.[] | select(.ServiceID | contains("registrator:mongod1r2:27017"))')
	MONGOD1R2IP=$(echo ${MONGOD1R2} | jq -r '.ServiceAddress')
	MONGOD1R2PORT=$(echo ${MONGOD1R2} | jq -r '.ServicePort')
	echo mongod1r2 ${MONGOD1R2IP}:${MONGOD1R2PORT}

	MONGOD2R2=$(echo $(curl -sb -H "Accept: application/json" "http://${MASTERIP}:8500/v1/catalog/service/mongo") \
		| jq -r '.[] | select(.ServiceID | contains("registrator:mongod2r2:27017"))')
	MONGOD2R2IP=$(echo ${MONGOD2R2} | jq -r '.ServiceAddress')
	MONGOD2R2PORT=$(echo ${MONGOD2R2} | jq -r '.ServicePort')
	echo mongod2r2 ${MONGOD2R2IP}:${MONGOD2R2PORT}

	MONGOD1R3=$(echo $(curl -sb -H "Accept: application/json" "http://${MASTERIP}:8500/v1/catalog/service/mongo") \
		| jq -r '.[] | select(.ServiceID | contains("registrator:mongod1r3:27017"))')
	MONGOD1R3IP=$(echo ${MONGOD1R3} | jq -r '.ServiceAddress')
	MONGOD1R3PORT=$(echo ${MONGOD1R3} | jq -r '.ServicePort')
	echo mongod1r3 ${MONGOD1R3IP}:${MONGOD1R3PORT}

	MONGOD2R3=$(echo $(curl -sb -H "Accept: application/json" "http://${MASTERIP}:8500/v1/catalog/service/mongo") \
		| jq -r '.[] | select(.ServiceID | contains("registrator:mongod2r3:27017"))')
	MONGOD2R3IP=$(echo ${MONGOD2R3} | jq -r '.ServiceAddress')
	MONGOD2R3PORT=$(echo ${MONGOD2R3} | jq -r '.ServicePort')
	echo mongod2r3 ${MONGOD2R3IP}:${MONGOD2R3PORT}
	
	echo "Waiting Generate js command for mongodb replication and sharding"
	docker run -i --rm -w /jsTmpl -v $(pwd)/mongo/jsTmpl:/jsTmpl -v $(pwd)/mongo/js:/js \
		-e mongod1r1=${ETH0IP} \
		-e mongod2r1=${ETH0IP} \
		-e mongod1r1Port=10001 \
		-e mongod2r1Port=10002 \
		-e mongod1r2=${MONGOD1R2IP} \
		-e mongod2r2=${MONGOD2R2IP} \
		-e mongod1r2Port=${MONGOD1R2PORT} \
		-e mongod2r2Port=${MONGOD2R2PORT} \
		-e mongod1r3=${MONGOD1R3IP} \
		-e mongod2r3=${MONGOD2R3IP} \
		-e mongod1r3Port=${MONGOD1R3PORT} \
		-e mongod2r3Port=${MONGOD2R3PORT} \
		ubuntu:14.04.1 /bin/bash /jsTmpl/start.sh
fi

# Uncomment to build mongo image yourself otherwise it will download from docker index.
echo "Waiting build jojo13572001/mongo images"
docker build -t jojo13572001/mongo ${LOCALPATH}/mongo

rm -rf ${LOCALPATH}/mongodata/*-$2
echo "Waiting setup sharding"
for (( i = 1; i <= $1; i++ )); do
	# Setup local db storage if not exist
	if [ ! -d "${LOCALPATH}/db/${i}-$2" ]; then
		mkdir -p ${LOCALPATH}/mongodata/${i}-$2
		#mkdir -p ${LOCALPATH}/mongodata/${i}-cfg
	fi
	# Create mongod servers
	echo "create mongod server${i}r$2"
	docker run --name mongod${i}r$2 -p $(($2*10000+${i})):27017 -i -d \
		-v ${LOCALPATH}/mongodata/${i}-$2:/data/db \
		-e OPTIONS="d --replSet set${i} --dbpath /data/db --notablescan --noprealloc --smallfiles --port 27017" jojo13572001/mongo
	if [ $2 -eq 1 ]; then
		sleep 20 
		# Setup replica set
		echo "start initialize replication set mongod${i}r1"
		docker run -P -i -t --rm \
			-e OPTIONS=" ${DOCKERIP}:$(($2*10000+${i})) /initiate.js" jojo13572001/mongo
		sleep 30 # Waiting for set to be initiated
		echo "start setup replication set mongod${i}r1"
		docker run -P -i -t --rm \
			-e OPTIONS=" ${DOCKERIP}:$(($2*10000+${i})) /setupReplicaSet${i}.js" jojo13572001/mongo
	fi
done
echo "Finish create mongod"
