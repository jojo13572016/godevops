# Base image is in https://registry.hub.docker.com/_/golang/
# Refer to https://blog.golang.org/docker for usage
FROM golang:1.5.1
MAINTAINER Bean jojo13572001@yahoo.com.tw

ARG APP_NAME
ENV GOPATH /go
ENV WORK_PATH /go/src/$APP_NAME
RUN mkdir -p $WORK_PATH
WORKDIR $WORK_PATH
COPY . $WORK_PATH

# Install beego & bee
RUN go get github.com/astaxie/beego
RUN go get github.com/beego/bee
RUN chmod +x docker-start.sh

EXPOSE 8080 3306

ENTRYPOINT ["./docker-start.sh"]
